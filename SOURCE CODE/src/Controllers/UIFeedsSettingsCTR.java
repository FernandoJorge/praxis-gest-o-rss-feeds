package Controllers;

import Dal.DataBaseAccess;
import Dal.FactoryDataBaseAccess;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * CLASS TO HANDLE REQUESTS FROM 'feddsrss.UIFeedsInfoSettings' class
 */
public class UIFeedsSettingsCTR {

    /**
     *
     * GETS THE CURRENT SETTINGS FOR SENDING EMAIL FEED UPDATE MESSAGES
     *
     * @return - Email settings(One of the MAP Object has a test record of
     * success : [key = 'result' => value = 'ack'] if success; [key = 'result'
     * => value = <other message>] if it fails)
     */
    public static Map<String, String> getSettings() {

        Map<String, String> settings = new HashMap<String, String>();

        // GETS MYSQL DATABASE CONNECTION OBJECT
        DataBaseAccess dbConnection = FactoryDataBaseAccess.getDataBaseAccessObject("MYSQL");

        if (dbConnection == null) {

            settings.put("result", "It was not possible to get a database access object...");
            return settings;
        }

        // OPENS DATABASE'S CONNECTION
        if (dbConnection.openConnection("localhost:3306", "praxis", "root", "")) {

            String SQL = "SELECT * FROM feed_information";

            ResultSet rs = dbConnection.select(SQL);

            if (rs == null) {

                dbConnection.closeConnection();
                settings.put("result", "It was not possible to get feeds information from database...");
                return settings;
            }

            try {
                while (rs.next()) {

                    settings.put("id", String.valueOf(rs.getInt("id")).trim());
                    settings.put("xml_path", rs.getString("feed_xml_path").trim());
                    settings.put("max_description", String.valueOf(rs.getInt("max_description")));
                    settings.put("min_perc_description", String.valueOf(rs.getFloat("min_perc_description")));
                    settings.put("validation_date", String.valueOf(rs.getInt("validation_date")));
                }
            } catch (Exception ex) {

                dbConnection.closeConnection();
                settings.put("result", "It was not possible to read feeds information...");
                return settings;
            }

            settings.put("result", "ack");

        } else {

            settings.put("result", "It was not possible to open a connection to database...");
            return settings;
        }

        // CLOSE DATABASE CONNECTION
        dbConnection.closeConnection();

        return settings;
    }

    /**
     *
     * SAVES THE NEW EMAIL SETTINGS FOR SENDING EMAIL FEED UPDATE MESSAGES
     *
     * @param settings - New email settings to be saved
     * @return 'ack' if success | Error message if it fails
     */
    public static String saveSettings(Map<String, String> settings) {

        // GETS MYSQL DATABASE CONNECTION OBJECT
        DataBaseAccess dbConnection = FactoryDataBaseAccess.getDataBaseAccessObject("MYSQL");

        if (dbConnection == null) {

            return "It was not possible to get a database access object...";
        }

        // OPENS DATABASE'S CONNECTION
        if (dbConnection.openConnection("localhost:3306", "praxis", "root", "")) {
            
            String SQL = "UPDATE feed_information "
                    + "SET feed_xml_path = '" + settings.get("xml_path") + "' , "
                    + "max_description = " + settings.get("max_description") + " , "
                    + "min_perc_description = " + settings.get("min_perc_description") + " , "
                    + "validation_date = " + Integer.parseInt(settings.get("validation_date")) + " "
                    + "WHERE id = " + settings.get("id");

            if (!dbConnection.update(SQL)) {

                dbConnection.closeConnection();
                return "It was not possible to update feeds settings in database...";
            }

        } else {

            return "It was not possible to open a connection to database...";
        }

        // CLOSE DATABASE CONNECTION
        dbConnection.closeConnection();

        return "ack";
    }
}
