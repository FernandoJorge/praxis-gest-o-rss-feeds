/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dal;

/**
 *
 * FACTORY CLASS TO CREATE A DATABASE OBJECT
 */
public class FactoryDataBaseAccess {

    /**
     * 
     * CREATES DATABASE OBEJCT TO HANDLE TRANSACTIONS
     * 
     * @param SGDB - DATABASE MANAGEMENT SYSTEM
     * @return - DATABASE OBJECT
     */
    public static DataBaseAccess getDataBaseAccessObject(String SGDB) {

        DataBaseAccess dbObj = null;

        try {
            dbObj = (DataBaseAccess) Class.forName("Dal." + SGDB + "_DB").newInstance();
        } catch (Exception ex) {

            System.out.println(ex.getCause());
            return dbObj;
        }

        return dbObj;
    }

}
