package Controllers;

import Utils.Email;
import Dal.DataBaseAccess;
import Dal.FactoryDataBaseAccess;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * CLASS TO HANDLE REQUESTS FROM 'feedsrss.UIEmailSettings' class
 */
public class UIEmailSettingsCTR {
    
    private static String[] sslAuthentications = {"NONE", "SSL/TLS", "STARTTLS"};

    /**
     * 
     * GETS AN ARRAY WITH SSL AUTHENTICATIONS TYPES
     * 
     * @return - Array with all the possible SSL authentications
     */
    public static String[] getSSLAuthentications() {

        return sslAuthentications;
    }

    /**
     * 
     * GETS THE CURRENT FEED SETTINGS
     * 
     * @return - Email settings(One of the MAP Object has a test record of success : [key = 'result' => value = 'ack'] if success; [key = 'result' => value = <other message>] if it fails)
     */
    public static Map<String, String> getSettings() {

        Map<String, String> settings = new HashMap<String, String>();

        // GETS MYSQL DATABASE CONNECTION OBJECT
        DataBaseAccess dbConnection = FactoryDataBaseAccess.getDataBaseAccessObject("MYSQL");

        if (dbConnection == null) {

            settings.put("result", "It was not possible to get a database access object...");
            return settings;
        }

        // OPENS DATABASE'S CONNECTION
        if (dbConnection.openConnection("localhost:3306", "praxis", "root", "")) {

            String SQL = "SELECT * FROM email_settings";

            ResultSet rs = dbConnection.select(SQL);

            if (rs == null) {

                dbConnection.closeConnection();
                settings.put("result", "It was not possible to get email settings from database...");
                return settings;
            }

            try {
                while (rs.next()) {

                    settings.put("id", String.valueOf(rs.getInt("id")).trim());
                    settings.put("from_address", rs.getString("from_address").trim());
                    settings.put("smtp_server", rs.getString("smtp_server").trim());
                    settings.put("smtp_port_number", String.valueOf(rs.getInt("smtp_port_number")).trim());
                    settings.put("ssl_authentication", rs.getString("ssl_authentication").trim());
                    settings.put("username", rs.getString("username").trim());
                    settings.put("password", rs.getString("password").trim());
                }
            } catch (Exception ex) {

                dbConnection.closeConnection();
                settings.put("result", "It was not possible to read email settings...");
                return settings;
            }

            settings.put("result", "ack");
        } else {
            settings.put("result", "It was not possible to open a connection to database...");
            return settings;
        }

        // CLOSE DATABASE CONNECTION
        dbConnection.closeConnection();

        return settings;

    }
    
    public static String testSMTPConnection(String fromAddress, String smtpServer, int port, String SSLAuthentication, String username, String password){
        
        Email email = new Email(fromAddress, smtpServer, port, SSLAuthentication, username, password);        
        String message = email.testSMTPConnection();
        
        return message;
    }

    /**
     * 
     * SAVES NEW FEED SETTINGS
     * 
     * @param settings - New Feed settings to save
     * @return - 'ack' if success | Error message if it fails
     */
    public static String saveSettings(Map<String, String> settings) {

        // GETS MYSQL DATABASE CONNECTION OBJECT
        DataBaseAccess dbConnection = FactoryDataBaseAccess.getDataBaseAccessObject("MYSQL");

        if (dbConnection == null) {

            return "It was not possible to get a database access object...";
        }

        // OPENS DATABASE'S CONNECTION
        if (dbConnection.openConnection("localhost:3306", "praxis", "root", "")) {

            String SQL = "UPDATE email_settings "
                    + "SET from_address = '" + settings.get("from_address") + "' , "
                    + "smtp_server = '" + settings.get("smtp_server") + "' , "
                    + "smtp_port_number = " + settings.get("smtp_port_number") + " , "
                    + "ssl_authentication = '" + settings.get("ssl_authentication") + "' , "
                    + "username = '" + settings.get("username") + "' , "
                    + "password = '" + settings.get("password") + "' "
                    + "WHERE id = " + settings.get("id");

            if (!dbConnection.update(SQL)) {

                dbConnection.closeConnection();
                return "It was not possible to update email settings in database...";
            }
        } else {
            return "It was not possible to open a connection to database...";
        }

        return "ack";
    }
}
