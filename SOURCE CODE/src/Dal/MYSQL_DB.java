package Dal;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * CLASS FOR MYSQL DATABASE TRANSACTIONS
 */
public class MYSQL_DB implements DataBaseAccess {

    private Connection connection;
    private Statement smt;
    private ResultSet rs;

    /**
     * 
     * OPENS A DATABASE CONNECTION
     * 
     * @param db_server - Server Address
     * @param db_database - Database name
     * @param db_username - Username for the database
     * @param db_password - Password for the databae
     * @return - false if occurrs an error | true if success
     */
    public boolean openConnection(String db_server, String db_database, String db_username, String db_password) {

        try {

            Class.forName("com.mysql.jdbc.Driver");

            this.connection = DriverManager.getConnection("jdbc:mysql://" + db_server + "/" + db_database, db_username, db_password);

        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * 
     * CLOSES DATABASE CONNECTION
     * 
     * @return - false if occurrs an error | true if success
     */
    public boolean closeConnection() {

        try {

            this.rs.close();
            this.smt.close();
            this.connection.close();
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * 
     * PERFORM A SELECT QUERY TO THE DATABASE
     * 
     * @param SQL - Query statement
     * @return - All records of the information required if success | 'null' if occurrs an error
     */
    public ResultSet select(String SQL) {

        try {

            this.smt = this.connection.createStatement();
            this.rs = smt.executeQuery(SQL);

        } catch (Exception ex) {

            this.rs = null;
            return this.rs;
        }

        return this.rs;
    }

    /**
     * 
     * PERFORM A UPDATE QUERY TO DATABASE
     * 
     * @param SQL - Query statement
     * @return - true if success | false if occurrs an error
     */
    public boolean update(String SQL) {

        try {

            this.smt = this.connection.createStatement();
            this.smt.executeUpdate(SQL);
        } catch (Exception ex) {
            return false;
        }

        return true;
    }
}
