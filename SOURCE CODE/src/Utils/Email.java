package Utils;

import java.util.*;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * CLASS TO HANDLE EMAIL MESSAGES
 */
public class Email {

    private int port;
    private String fromAddress;
    private String smtpServer;
    private String SSLAuthentication;
    private String username;
    private String password;

    /**
     *
     * CLASS CONSTRUCTOR
     *
     * @param fromAddress - Email address for the message sender
     * @param smtpServer - Simple Mail Transfer protocol Server's address
     * @param port - Port number for the SMTP server
     * @param SSLAuthentication - SSL Authetication type
     * @param username - Username for authentication
     * @param password - Password for authentication
     */
    public Email(String fromAddress, String smtpServer, int port, String SSLAuthentication, String username, String password) {

        this.fromAddress = fromAddress;
        this.smtpServer = smtpServer;
        this.port = port;
        this.SSLAuthentication = SSLAuthentication;
        this.username = username;
        this.password = password;
    }

    /**
     *
     * SENDS AN EMAIL MESSAGE TO VARIOUS RECIPIENTS
     *
     * @param toAddress - List of recipient´s email addresses for email's header
     * @param subject - Subject for email's header
     * @param body - Content of email message(Prepared only for HTML content)
     * @return 'ack' if success | 'Error' message if occurs an error
     */
    public String sendEmail(String[] toAddress, String subject, String body) {

        // GETS PROPERTIES OBJECT
        Properties properties = new Properties();

        // SETS EMAIL SERVER
        properties.put("mail.smtp.host", this.smtpServer);
        properties.put("mail.smtp.port", this.port);
        properties.put("mail.smtp.auth", true);

        // SETS AUTHENTICATION IF NEEDED
        Authentication authenticator = authenticator = new Authentication(this.username, this.password);

        if (this.SSLAuthentication.equals("SSL/TLS")) {

            properties.put("mail.smtp.ssl.enable", true);
        }

        if (this.SSLAuthentication.equals("STARTTLS")) {

            properties.put("mail.smtp.starttls.enable", true);
            // In the case of using SSL
            //properties.put("mail.smtp.socketFactory.port", this.port);
            //properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            //properties.put("mail.smtp.socketFactory.fallback", "false");

        }

        // GETS DEFAULT SESSION OBJECT
        Session session = Session.getInstance(properties, authenticator);

        // CREATES A DEFAULT MIME MESSAGE OBJECT
        MimeMessage message = new MimeMessage(session);

        // CREATES A MULTIPART OBJECT FOR TEXT & HTML CONTENT
        Multipart multiPart = new MimeMultipart("MultiPart");

        try {

            // SETS SENDER´S ADDRESS
            message.setFrom(new InternetAddress(this.fromAddress));

            // ADD ALL THE RECIPIENTS ADDRESSES
            for (int i = 0; i < toAddress.length; i++) {

                message.addRecipient(Message.RecipientType.BCC, new InternetAddress(toAddress[i]));
            }

            // SETS THE SUBJECT OF THE EMAIL
            message.setSubject(subject);

            // SET SEND DATE MESSAGE
            message.setSentDate(new Date());

            // HTML PART
            MimeBodyPart HTML = new MimeBodyPart();
            HTML.setContent(body, "text/html");

            // SETS THE BODY OF THE EMAIL
            multiPart.addBodyPart(HTML);
            message.setContent(multiPart);

            // SEND EMAIL MESSAGE
            Transport.send(message);

        } catch (Exception ex) {

            return "Occurred an error while sending email message feed update...\n\nError:\n\n" + ex.getCause();
        }

        return "ack";
    }

    /**
     *
     * TESTS IF SMTP SETTINGS ARE CORRECT FOR CONNECTION
     *
     * @return 'ack' if success | 'error' message if it fails
     */
    public String testSMTPConnection() {

        try {
            Properties properties = new Properties();
            // required for gmail 

            properties.put("mail.smtp.auth", true);
            properties.put("mail.smtp.timeout", 30000);

            // SETS AUTHENTICATION IF NEEDED
            Authentication authenticator = new Authentication(this.username, this.password);

            if (this.SSLAuthentication.equals("SSL/TLS")) {

                properties.put("mail.smtp.ssl.enable", true);
            }

            if (this.SSLAuthentication.equals("STARTTLS")) {

                properties.put("mail.smtp.starttls.enable", true);
            }

            Session session = Session.getInstance(properties, authenticator);
            Transport transport = session.getTransport("smtp");
            transport.connect(this.smtpServer, this.port, this.username, this.password);
            transport.close();
            return "ack";
        } catch (AuthenticationFailedException e) {

            return "Authentication failed...\n\nError:\n\n" + e.getMessage();
        } catch (MessagingException e) {

            return "Connection failed...\n\nError:\n\n" + e.getMessage();
        }
    }

    /**
     * CLASS TO HANDLE AUTHETICATION CREDENTIALS
     */
    private class Authentication extends Authenticator {

        private PasswordAuthentication pa;

        /**
         *
         * @param username - Authentication username
         * @param password - Authentication password
         */
        public Authentication(String username, String password) {

            pa = new PasswordAuthentication(username, password);
        }

        /**
         *
         * GETS A 'PasswordAuthentication' OBJECT
         *
         * @return - PasswordAuthentication Object
         */
        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            return pa;
        }
    }

}
