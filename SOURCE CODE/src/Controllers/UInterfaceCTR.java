package Controllers;

import Dal.DataBaseAccess;
import Dal.FactoryDataBaseAccess;
import Utils.Email;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * CLASS CONTROLLER TO HANDLE REQUESTs FROM 'feedsrss.UInterface' class
 */
public class UInterfaceCTR {

    /**
     *
     * SENDS A FEED UPDATE BASED ON DATE RANGE
     *
     * @param fromDate - Start date for submited proposals
     * @param toDate - End date for submited proposals
     * @return - 'ack' message if success | Error message if it fails
     */
    public static String sendFeed(String fromDate, String toDate, String[] subscribers) {

        // GETS MYSQL DATABASE CONNECTION OBJECT
        DataBaseAccess dbConnection = FactoryDataBaseAccess.getDataBaseAccessObject("MYSQL");

        if (dbConnection == null) {

            return "It was not possible to get a database access object...";
        }

        // OPENS DATABASE'S CONNECTION
        if (dbConnection.openConnection("localhost:3306", "praxis", "root", "")) {

            // GETS ALL THE PROPOSALS BETWEEN THE DATES PASSED BY ARGUMENT
            String SQL = "SELECT id , title , description FROM proposal "
                    + "WHERE (submitDate BETWEEN '" + fromDate + "' AND '" + toDate + "') AND validTo > CURDATE() "
                    + "ORDER BY submitDate ASC";

            ResultSet rs = dbConnection.select(SQL);

            if (rs == null) {
                dbConnection.closeConnection();
                return "It was not possible to get proposals from database...";
            }

            int size = 0;

            try {
                rs.last();
                size = rs.getRow();
                rs.beforeFirst();
            } catch (Exception ex) {

                dbConnection.closeConnection();
                return "Occurred an error while geting the number of projects/internships for this request...";
            }

            if (size == 0) {

                dbConnection.closeConnection();
                return "There's no projects/internships records to send feed update with the date range inserted...\nInsert a different date range.";
            }

            // GETS MIN AND MAX NUMBER OF CHARACTERS FOR DESCRIPTION
            SQL = "SELECT * FROM feed_information";

            ResultSet rs2 = dbConnection.select(SQL);

            if (rs2 == null) {
                dbConnection.closeConnection();
                return "It was not possible to get feed information settings from database...";
            }

            int maxDescription = -1;
            float porcMinDescription = -1;

            try {
                while (rs2.next()) {

                    maxDescription = rs2.getInt("max_description");
                    porcMinDescription = rs2.getFloat("min_perc_description");
                }
            } catch (SQLException ex) {

                dbConnection.closeConnection();
                return "It was not possible to read feed information settings...";
            }

            if (maxDescription == -1 || porcMinDescription == -1) {

                dbConnection.closeConnection();
                return "The feed information settings is insuficient to send feed update...\nEdit 'FEED INFORMATION SETTINNGS' correctly.";
            }

            // CREATES HTML EMAIL UPDATE CONTENT
            String htmlMailContent = createHtmlUpdateContent(rs, maxDescription, porcMinDescription);

            if (htmlMailContent.equals("error")) {

                dbConnection.closeConnection();
                return "IT was not possible to generate email message content to send feed update...";
            }

            // GETS EMAIL SETTINGS
            int port = -1;
            String fromAddress = "";
            String smtpServer = "";
            boolean authentication = false;
            String SSLAuthentication = "";
            String username = "";
            String password = "";

            SQL = "SELECT * FROM email_settings";

            rs = dbConnection.select(SQL);

            if (rs == null) {
                dbConnection.closeConnection();
                return "It was not possible to get email setings from database to send feed update...";
            }

            try {
                while (rs.next()) {

                    port = rs.getInt("smtp_port_number");
                    fromAddress = rs.getString("from_address").trim();
                    smtpServer = rs.getString("smtp_server").trim();
                    SSLAuthentication = rs.getString("ssl_authentication").trim();
                    username = rs.getString("username").trim();
                    password = rs.getString("password").trim();
                }
            } catch (Exception ex) {

                dbConnection.closeConnection();
                return "Occurred an error reading email settings to send feed update...";
            }

            if (fromAddress.equals("") || smtpServer.equals("") || port == -1 || SSLAuthentication.equals("")) {

                dbConnection.closeConnection();
                return "The email settings information is insuficient to send feed update...\nEdit 'EMAIL SETTINNGS' correctly.";
            }

            // SEND EMAIL MESSAGE
            Email email = new Email(fromAddress, smtpServer, port, SSLAuthentication, username, password);

            String result = email.sendEmail(subscribers, "PRAXIS Projects/Internships Update", htmlMailContent);

            if (!result.equals("ack")) {

                dbConnection.closeConnection();
                return "Occurred an error while sending email message feed update...";
            }

        } else {

            return "It was not possible to open a connection to database...";
        }

        // CLOSE DATABASE CONNECTION
        dbConnection.closeConnection();

        return "ack";
    }

    /**
     *
     * GENERATE HTML CONTENT TO FOR EMAIL MESSAGE OF THE FEED UPDATE
     *
     * @param rs - All the records of the proposals for the feed update
     * @return - Html content if success | 'Error' message if it fails
     */
    private static String createHtmlUpdateContent(ResultSet rs, int maxDescription, float porcMinDescription) {

        String mailContent = "<!DOCTYPE html>"
                + "<html>"
                + "<head>"
                + "<meta charset='UTF-8'>"
                + "<meta name='viewport' content='width=device-width, initial-scale=1.0'>"
                + "<title>PRAXIS PROJECT/INTERSHIPS UPDATE</title>"
                + "</head>"
                + "<body>"
                + "<h1>PRAXIS PROJECTS/INTERNSHIPS UPDATE</h1>"
                + "<br><br><br>";

        try {
            while (rs.next()) {

                String title = rs.getString("title");

                if (title == null) {
                    title = "THERE'S NO TITLE AVAILABLE";
                }

                String link = "http://www.praxisnetwork.eu/proposal/id/" + rs.getInt("id");
                String description = rs.getString("description");

                if (description != null) {
                    description = description.replaceAll("\\<.*?\\>", "");
                    int strSize = description.length();

                    if (strSize >= maxDescription) {

                        description = description.substring(0, maxDescription - 1);
                    } else {

                        int endIndex = (int) (porcMinDescription * strSize);
                        description = description.substring(0, endIndex);
                    }
                } else {

                    description = "THERE'S NO DESCRIPTION AVAILABLE";
                }

                mailContent += "<h2><u>" + title + "</u></h2>"
                        + "<p><b>" + description + "...(click the link below)</b></p>"
                        + "<br><br>"
                        + "<a href='" + link + "' target='_blank'>See full proposal</a>"
                        + "<br><br>"
                        + "<hr>";
            }
        } catch (Exception ex) {

            System.out.println(ex.getCause());
            return "error";
        }

        mailContent += "</body>"
                + "</html>";

        return mailContent;
    }

    /**
     *
     * GETS ALL SUBSCRIBERS
     *
     * @return - If success, string array with all subscribers´ emails |
     * 'subscribersConnectionObjectError' in the first position of the string
     * array if database access object instance gives error |
     * 'subscribersConnectionError' in the first position of string the array if
     * can´t open a connection to database | 'subscribersError' in the first
     * position of the string array if occurs an error while reading the
     * subscribers´ emails | 'subscribersNoInfo' in the first position of the
     * string array if there´s no subscribers´ emails in the database.
     */
    public static String[] getSubscribers() {

        String[] subscribers = null;

        // GETS MYSQL DATABASE CONNECTION OBJECT
        DataBaseAccess dbConnection = FactoryDataBaseAccess.getDataBaseAccessObject("MYSQL");

        if (dbConnection == null) {

            subscribers = new String[1];
            subscribers[0] = "subscribersConnectionObjectError";

            return subscribers;
        }

        // OPENS DATABASE'S CONNECTION
        if (dbConnection.openConnection("localhost:3306", "praxis", "root", "")) {

            String SQL = "SELECT email FROM feed_community ORDER BY email ASC";

            ResultSet rs = dbConnection.select(SQL);

            if (rs == null) {

                dbConnection.closeConnection();
                subscribers = new String[1];
                subscribers[0] = "subscribersError";

                return subscribers;
            }

            ArrayList<String> subs = new ArrayList<String>();

            try {
                while (rs.next()) {

                    subs.add(rs.getString("email").trim());
                }
            } catch (SQLException ex) {

                dbConnection.closeConnection();
                subscribers = new String[1];
                subscribers[0] = "subscribersError";

                return subscribers;
            }

            if (subs.size() > 0) {

                subscribers = new String[subs.size()];

                for (int i = 0; i < subs.size(); i++) {

                    subscribers[i] = subs.get(i);
                }

            } else {

                dbConnection.closeConnection();
                subscribers = new String[1];
                subscribers[0] = "subscribersNoInfo";
            }
        } else {

            dbConnection.closeConnection();
            subscribers = new String[1];
            subscribers[0] = "subscribersConnectionError";
        }

        dbConnection.closeConnection();
        return subscribers;
    }

}
